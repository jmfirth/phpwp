name 'dev'

default_attributes 'wordpress' => {
  'db' => {
    'root_password' => 'r@@tp4sswd',
    'instance_name' => 'main',
    'name' => 'wpmain',
    'user' => 'wpmainuser'
  }
}
