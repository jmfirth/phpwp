# Installation

## Repository Preparation

First, install Ruby on the host (2.1.5/2.1.6)

```bash
choco install ruby
```

Then install Chef
```bash
choco install chef
```

Check out the repository.  Prepare the repository:

```bash
cd env
gem install bundler
bundle install
librarian-chef install
```

## Vagrant

### Virtualbox

```bash
cd env
vagrant up --provider virtualbox
```

### AWS

TODO using https://github.com/mitchellh/vagrant-aws

## Knife Solo

```bash
cd env
# Prepare the remote server for Chef
knife solo prepare user@host {-p <port#>} -VV

# Check the Chef version running on the remote server
knife solo prepare user@host {-p <port#>} -VV --version

# Upload the cookbook library and provision
knife solo cook user@host {-p <port#>} -VV
```

### Notes

- Seems to work flawlessly with Ubuntu 14.04(.03)
